package pruebaml.example.com.pruebaml.model

import com.google.gson.annotations.SerializedName
import pruebaml.example.com.pruebaml.utils.Utils
import java.io.Serializable

data class Product(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("price")
    val price: Double,
    @SerializedName("currency_id")
    val currency_id: String,
    @SerializedName("thumbnail")
    val thumbnail: String) : Serializable
{
    fun getPriceWithCurrency():String{
        return Utils.getPriceWithCurrency(price,currency_id)
    }
}