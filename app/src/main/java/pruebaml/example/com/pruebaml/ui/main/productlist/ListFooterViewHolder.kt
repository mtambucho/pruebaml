package pruebaml.example.com.pruebaml.ui.main.productlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_list_footer.view.*
import pruebaml.example.com.pruebaml.R
import pruebaml.example.com.pruebaml.enum.StateEnum



class ListFooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(status: StateEnum?) {
        itemView.progress_bar.visibility = if (status == StateEnum.LOADING) View.VISIBLE else View.INVISIBLE
        itemView.txt_error.visibility = if (status == StateEnum.ERROR) View.VISIBLE else View.INVISIBLE
    }

    companion object {
        fun create(retry: () -> Unit, parent: ViewGroup): ListFooterViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_footer, parent, false)
            view.txt_error.setOnClickListener { retry() }
            return ListFooterViewHolder(view)
        }
    }
}
