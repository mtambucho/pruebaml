package pruebaml.example.com.pruebaml.ui.main.productlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import pruebaml.example.com.pruebaml.MainActivity
import pruebaml.example.com.pruebaml.R
import pruebaml.example.com.pruebaml.model.Product


class ProductListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    // bind the product, click listener
    fun bind(product: Product?) {
        if (product != null) {
            itemView.txt_product_title.text = product.title
            itemView.txt_product_price.text = product.getPriceWithCurrency()

            //load image
            Picasso.get().load(product.thumbnail).into(itemView.img_product)

            //click product listener
            itemView.card_view.setOnClickListener {
                context.navigateToProductDetailFragment(product.id)
            }
        }
    }


    companion object {
        lateinit var context: MainActivity
        fun create(parent: ViewGroup): ProductListViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_product, parent, false)
            context = parent.context as MainActivity
            return ProductListViewHolder(view)
        }

    }

}