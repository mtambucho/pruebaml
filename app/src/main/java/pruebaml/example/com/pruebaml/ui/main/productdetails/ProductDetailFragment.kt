package pruebaml.example.com.pruebaml.ui.main.productdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.product_detail.*
import kotlinx.android.synthetic.main.product_main_bar.*
import pruebaml.example.com.pruebaml.R
import pruebaml.example.com.pruebaml.api.NetworkService
import retrofit2.HttpException
import java.net.HttpURLConnection


class ProductDetailFragment : Fragment() {

    private val networkService = NetworkService.getService()
    private val compositeDisposable = CompositeDisposable()

    private var retryCompletable: Completable? = null
    private lateinit var viewModel: ProductDetailViewModel


    private lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.product_detail, container, false)
        return rootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductDetailViewModel::class.java)
        val idProduct = ProductDetailFragmentArgs.fromBundle(arguments).productIdArgument
        loading(true)
        getDetails(idProduct)

    }


    private fun getDetails(id : String) {
        compositeDisposable.add(networkService.getProductDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { response ->
                        viewModel.product = response
                        initContent()
                        loading(false)

                    },
                    {
                        if (it is HttpException){
                            if (it.code() == HttpURLConnection.HTTP_NOT_FOUND){

                            }
                        }
                        it.printStackTrace()
                        setRetry(Action { getDetails(id) })
                    }
                ))

        compositeDisposable.add(networkService.getProductDescription(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    viewModel.description = response
                    updateDescription()
                },
                {
                    /// in all cases we use same behavior
                    viewModel.description = null
                    updateDescription()
                }
            ))

    }

    private fun initContent() {
        //load initial image
        Picasso.get().load(viewModel.product.pictures[0].url).error(R.drawable.default_thumb).into(product_image)
        product_title.text = viewModel.product.title
        product_price.text = viewModel.product.getPriceWithCurrency()
        loading(false)
    }

    private fun updateDescription(){
        val description = rootView.findViewById<TextView>(R.id.description)
        if (viewModel.description != null  && !viewModel.description!!.plain_text.isEmpty()){
            description.text = viewModel.description!!.plain_text
        }else{
            description.text = getString(R.string.no_description_product)
        }
        description_progress_bar.visibility = View.GONE
    }


    fun loading(loading: Boolean){
        if (loading){
            progress_bar.visibility = View.VISIBLE
            product_main_frame.visibility = View.GONE
        }else{
            progress_bar.visibility = View.GONE
            product_main_frame.visibility = View.VISIBLE
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }


}