package pruebaml.example.com.pruebaml.ui.main.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import pruebaml.example.com.pruebaml.MainActivity
import pruebaml.example.com.pruebaml.R
import pruebaml.example.com.pruebaml.ui.main.MainViewModel

class SearchFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var rootView: View
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.search_fragment, container, false)
        retainInstance = true
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
        setupSearchView()
    }

    //setup the search view
    private fun setupSearchView() {
        val searchView = rootView.findViewById<SearchView>(R.id.search_view)

        //search view not iconified
        searchView.setIconifiedByDefault(false)
        searchView.isIconified = false

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                searchView.clearFocus()
                searchView.isIconified = true
                mainActivity.navigateToProductListFragment(s)
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                return false
            }
        })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as MainActivity
    }

}
