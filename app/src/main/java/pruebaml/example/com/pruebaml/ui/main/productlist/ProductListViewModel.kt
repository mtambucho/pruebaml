package pruebaml.example.com.pruebaml.ui.main.productlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import pruebaml.example.com.pruebaml.api.NetworkService
import pruebaml.example.com.pruebaml.enum.StateEnum
import pruebaml.example.com.pruebaml.model.Product
import pruebaml.example.com.pruebaml.utils.Constants


class ProductListViewModel (q : String) : ViewModel() {

    private val networkService = NetworkService.getService()
    var productList: LiveData<PagedList<Product>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = Constants.PRODUCT_LIST_PAGE_SIZE
    private val productDataSourceFactory: ProductDataSourceFactory
    var query: String = q

    init {
        productDataSourceFactory = ProductDataSourceFactory(
            compositeDisposable,
            networkService,
            query
        )
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        productList = LivePagedListBuilder<Int, Product>(productDataSourceFactory, config).build()
    }


    fun getState(): LiveData<StateEnum> = Transformations.switchMap<ProductDataSource,
            StateEnum>(productDataSourceFactory.productDataSourceLiveData, ProductDataSource::state)

    fun retry() {
        productDataSourceFactory.productDataSourceLiveData.value?.retry()
    }

    fun listIsEmpty(): Boolean {
        return productList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}