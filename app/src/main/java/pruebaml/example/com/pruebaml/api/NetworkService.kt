package pruebaml.example.com.pruebaml.api

import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pruebaml.example.com.pruebaml.model.ProductDescription
import pruebaml.example.com.pruebaml.model.ProductDetails
import pruebaml.example.com.pruebaml.model.SearchProductResponse
import pruebaml.example.com.pruebaml.utils.Constants
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkService {

    // search product list with name "q"
    @GET("sites/MLU/search?")
    fun getProducts(
        @Query("q") query: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Single<SearchProductResponse>

    // get product details
    @GET("items/{item_id}")
    fun getProductDetails(
        @Path("item_id") item_id: String
    ): Single<ProductDetails>

    // get product description
    @GET("items/{item_id}/description")
    fun getProductDescription(
        @Path("item_id") item_id: String
    ): Single<ProductDescription>

    // api configuration
    companion object {
        fun getService(): NetworkService {

            val interceptor = HttpLoggingInterceptor()

            //log the api calls
            if (Constants.LOG_API){
                interceptor.level = HttpLoggingInterceptor.Level.BODY
            }else{
                interceptor.level = HttpLoggingInterceptor.Level.NONE
            }

            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()


            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
                .create()
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            return retrofit.create(NetworkService::class.java)
        }
    }
}