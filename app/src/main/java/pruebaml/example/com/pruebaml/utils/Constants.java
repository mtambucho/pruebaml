package pruebaml.example.com.pruebaml.utils;


public class Constants {
    // API URL
    public static final String API_URL = "https://api.mercadolibre.com/";

    // size for product list pages
    public static final int PRODUCT_LIST_PAGE_SIZE = 20;

    // log the API calls
    public static final boolean LOG_API = true;
}
