package pruebaml.example.com.pruebaml.ui.main.productlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


@Suppress("UNCHECKED_CAST")
class ProductListViewModelFactory (
    private val query: String
): ViewModelProvider.NewInstanceFactory() {
    override fun <T: ViewModel> create(modelClass:Class<T>): T {
        return ProductListViewModel(query) as T
    }
}
