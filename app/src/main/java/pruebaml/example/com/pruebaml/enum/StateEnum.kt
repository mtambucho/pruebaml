package pruebaml.example.com.pruebaml.enum

enum class StateEnum {
    DONE, LOADING, ERROR
}