package pruebaml.example.com.pruebaml.model

import com.google.gson.annotations.SerializedName
import pruebaml.example.com.pruebaml.utils.Utils
import java.io.Serializable

data class ProductDetails(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("price")
    val price: Double,
    @SerializedName("pictures")
    val pictures: ArrayList<Picture>,
    @SerializedName("currency_id")
    val currency_id: String
): Serializable {

    fun getPriceWithCurrency() : String{
       return Utils.getPriceWithCurrency(price,currency_id)
    }
}

data class Picture(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String
):Serializable