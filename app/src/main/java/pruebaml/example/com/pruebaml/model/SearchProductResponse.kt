package pruebaml.example.com.pruebaml.model

import com.google.gson.annotations.SerializedName

data class SearchProductResponse(
    @SerializedName("site_id")
    val site_id: String,
    @SerializedName("results")
    val products: ArrayList<Product>
)