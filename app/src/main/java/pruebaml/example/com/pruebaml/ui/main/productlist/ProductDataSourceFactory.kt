package pruebaml.example.com.pruebaml.ui.main.productlist

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable
import pruebaml.example.com.pruebaml.api.NetworkService
import pruebaml.example.com.pruebaml.model.Product

class ProductDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val networkService: NetworkService,
    private var query:String)
    : DataSource.Factory<Int, Product>() {

    val productDataSourceLiveData = MutableLiveData<ProductDataSource>()

    override fun create(): DataSource<Int, Product> {
        val productDataSource = ProductDataSource(
            networkService,
            compositeDisposable,
            query
        )
        productDataSourceLiveData.postValue(productDataSource)
        return productDataSource
    }
}