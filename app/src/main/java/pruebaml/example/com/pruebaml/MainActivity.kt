package pruebaml.example.com.pruebaml

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI
import pruebaml.example.com.pruebaml.ui.main.productlist.ProductListFragmentDirections
import pruebaml.example.com.pruebaml.ui.main.search.SearchFragmentDirections

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        setupNavigation()
    }

    // setup navigation controller with back button
    private fun setupNavigation() {
        navController = findNavController(this, R.id.container)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    // go to productList Fragment
    fun navigateToProductListFragment(query: String) {
        val action = SearchFragmentDirections.actionSearchToProductlist(query)
        navController.navigate(action)
    }

    // go to productList Fragment
    fun navigateToProductDetailFragment(id : String) {
        val action = ProductListFragmentDirections.actionProductlistToProductdetail(id)
        navController.navigate(action)
    }


    // setup back button navigation
    override fun onSupportNavigateUp() = navController.navigateUp()

    //update the action bar title
    fun updateTitle(title: String) {
        supportActionBar!!.title = title
    }

}
