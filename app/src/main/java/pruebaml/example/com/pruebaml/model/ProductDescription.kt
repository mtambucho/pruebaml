package pruebaml.example.com.pruebaml.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProductDescription(
    @SerializedName("plain_text")
    val plain_text: String
) : Serializable
