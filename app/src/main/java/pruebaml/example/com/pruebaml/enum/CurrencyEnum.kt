package pruebaml.example.com.pruebaml.enum

enum class CurrencyEnum(val id: String,val value : String) {
    PESOS_URU("UYU", "$"),
    DOLARES("USD", "U$"),
    PESOS_ARG("ARS", "$"),
    DEFAULT_CURRENCY("DEFAULT", "$")
}