package pruebaml.example.com.pruebaml.utils

import android.util.Log
import pruebaml.example.com.pruebaml.enum.CurrencyEnum
import pruebaml.example.com.pruebaml.enum.ErrorEnum
import kotlin.math.roundToInt


/**
 * Created by MartinTambucho on 18/5/16.
 */
object Utils
{

    // get price with currency string (ej: 44.99, UYU -> $45)
    fun getPriceWithCurrency(price:Double, currency:String): String {
        var roundedPrice: String = try {
            price.roundToInt().toString()
        }catch (e: Exception){
            printLog(ErrorEnum.PRICE_CONVERSION_ERROR, "$currency $price")
            price.toString()
        }
        return when (currency) {
            CurrencyEnum.PESOS_URU.id -> CurrencyEnum.PESOS_URU.value + " " + roundedPrice
            CurrencyEnum.DOLARES.id -> CurrencyEnum.DOLARES.value + " " + roundedPrice
            CurrencyEnum.PESOS_ARG.id -> CurrencyEnum.PESOS_ARG.value + " " + roundedPrice
            else -> CurrencyEnum.DEFAULT_CURRENCY.value + " " + roundedPrice
        }
    }

    fun printLog(message:String){
        Log.i("PruebaML Error ->", "Error: $message")
    }

    fun printLog(errorEnum: ErrorEnum, extraMessage: String){
        printLog(extraMessage + " - " +errorEnum.descriptionToLog)
    }

    fun printLog(errorEnum: ErrorEnum){
            printLog(errorEnum.descriptionToLog)
    }
}
