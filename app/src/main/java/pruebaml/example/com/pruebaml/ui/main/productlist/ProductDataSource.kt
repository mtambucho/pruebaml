package pruebaml.example.com.pruebaml.ui.main.productlist

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import pruebaml.example.com.pruebaml.api.NetworkService
import pruebaml.example.com.pruebaml.enum.StateEnum
import pruebaml.example.com.pruebaml.model.Product

class ProductDataSource(
    private val networkService: NetworkService,
    private val compositeDisposable: CompositeDisposable,
    private var query: String
)
    : PageKeyedDataSource<Int, Product>() {

    private val FIRST_PAGE = 0

    var state: MutableLiveData<StateEnum> = MutableLiveData()
    private var retryCompletable: Completable? = null

    //load the initial page
    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Product>) {
        updateState(StateEnum.LOADING)
        compositeDisposable.add(
            networkService.getProducts(query ,FIRST_PAGE, params.requestedLoadSize)
                .subscribe(
                    { response ->
                        updateState(StateEnum.DONE)
                        callback.onResult(response.products,
                            null,
                            FIRST_PAGE + 1
                        )
                    },
                    {
                        updateState(StateEnum.ERROR)
                        setRetry(Action { loadInitial(params, callback) })
                    }
                )
        )
    }

    //load more pages
    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        updateState(StateEnum.LOADING)
        compositeDisposable.add(
            networkService.getProducts(query ,params.key, params.requestedLoadSize)
                .subscribe(
                    { response ->
                        updateState(StateEnum.DONE)
                        callback.onResult(response.products,
                            params.key + 1
                        )
                    },
                    {
                        updateState(StateEnum.ERROR)
                        setRetry(Action { loadAfter(params, callback) })
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
    }

    private fun updateState(state: StateEnum) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}