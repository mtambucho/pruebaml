package pruebaml.example.com.pruebaml.ui.main.productlist

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.product_list.*
import pruebaml.example.com.pruebaml.MainActivity
import pruebaml.example.com.pruebaml.R
import pruebaml.example.com.pruebaml.enum.StateEnum

class ProductListFragment : Fragment() {

    private lateinit var viewModel: ProductListViewModel
    private lateinit var productListAdapter: ProductListAdapter
    private lateinit var mainActivity: MainActivity


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.product_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val query = ProductListFragmentArgs.fromBundle(arguments).queryArgument
        viewModel = ViewModelProviders.of(
            this,
            ProductListViewModelFactory(query)
        ).get(ProductListViewModel::class.java)
        mainActivity.updateTitle(viewModel.query)
        initAdapter()
        initState()
    }

    // initialize adapter and observe productList changes
    private fun initAdapter() {
        productListAdapter = ProductListAdapter { viewModel.retry() }
        recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_view.adapter = productListAdapter

        //observe the product list changes and bind it
        viewModel.productList.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                //list is empty create alert dialog and go to search view
                listIsEmpty()
            } else {
                //list is not empty, submit
                productListAdapter.submitList(it)
            }
        })
    }

    //create alert for empty list
    private fun listIsEmpty() {
        val dialog = AlertDialog.Builder(mainActivity).setTitle(getString(R.string.no_products_found)).setMessage(
            getString(
                R.string.check_the_search
            )
        )
            .setPositiveButton(getString(R.string.ok)) { _: DialogInterface, _: Int ->
                run {
                    mainActivity.onBackPressed()
                }
            }
        dialog.show()
    }


    private fun initState() {
        //retry when user click on error screen
        txt_error.setOnClickListener { viewModel.retry() }

        //observe the state change
        viewModel.getState().observe(this, Observer { state ->
            progress_bar.visibility =
                    if (viewModel.listIsEmpty() && state == StateEnum.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (viewModel.listIsEmpty() && state == StateEnum.ERROR) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                productListAdapter.setState(state ?: StateEnum.DONE)
            }
        })
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as MainActivity

    }

    override  fun onResume() {
        super.onResume()
        mainActivity.updateTitle(viewModel.query)
    }

    override fun onStart() {
        super.onStart()
        mainActivity.updateTitle(viewModel.query)

    }
}
