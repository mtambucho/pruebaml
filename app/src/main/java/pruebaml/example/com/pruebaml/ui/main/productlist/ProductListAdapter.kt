package pruebaml.example.com.pruebaml.ui.main.productlist
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pruebaml.example.com.pruebaml.enum.StateEnum
import pruebaml.example.com.pruebaml.model.Product


class ProductListAdapter (private val retry: () -> Unit)
    : PagedListAdapter<Product, RecyclerView.ViewHolder>(ProductDiffCallback) {

    private val PRODUCT_TYPE = 1
    private val FOOTER_TYPE = 2

    private var state = StateEnum.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == PRODUCT_TYPE) ProductListViewHolder.create(
            parent
        ) else ListFooterViewHolder.create(retry, parent)
    }

    //PRODUCT_TYPE create item product -  FOOTER_TYPE create footer
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == PRODUCT_TYPE)
            (holder as ProductListViewHolder).bind(getItem(position))
        else (holder as ListFooterViewHolder).bind(state)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) PRODUCT_TYPE else FOOTER_TYPE
    }

    companion object {
        val ProductDiffCallback = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == StateEnum.LOADING || state == StateEnum.ERROR)
    }

    fun setState(state: StateEnum) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


}
