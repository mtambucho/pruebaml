package pruebaml.example.com.pruebaml


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest2() {
        val textView = onView(
            allOf(
                withText("Mercado Libre"),
                childAtPosition(
                    allOf(
                        withId(R.id.action_bar),
                        childAtPosition(
                            withId(R.id.action_bar_container),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Mercado Libre")))

        val linearLayout = onView(
            allOf(
                withId(R.id.search_edit_frame),
                childAtPosition(
                    allOf(
                        withId(R.id.search_bar),
                        childAtPosition(
                            withId(R.id.search_view),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        Thread.sleep(1000)

        val searchAutoComplete = onView(
            allOf(
                withId(R.id.search_src_text),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete.perform(replaceText("pelota"), closeSoftKeyboard())

        val searchAutoComplete2 = onView(
            allOf(
                withId(R.id.search_src_text), withText("pelota"),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete2.perform(pressImeActionButton())

        val cardView = onView(
            allOf(
                withId(R.id.card_view),
                childAtPosition(
                    allOf(
                        withId(R.id.recycler_view),
                        childAtPosition(
                            withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                            2
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        Thread.sleep(7000)
        cardView.perform(click())

        val button = onView(
            allOf(
                withId(R.id.purchase_button),
                childAtPosition(
                    allOf(
                        withId(R.id.main_bar_constraint),
                        childAtPosition(
                            withId(R.id.product_main_frame),
                            0
                        )
                    ),
                    3
                ),
                isDisplayed()
            )
        )
        Thread.sleep(5000)
        button.check(matches(isDisplayed()))


        val textView2 = onView(
            allOf(
                withId(R.id.title_description), withText("Descripción"),
                childAtPosition(
                    allOf(
                        withId(R.id.main_bar_constraint),
                        childAtPosition(
                            withId(R.id.product_main_frame),
                            0
                        )
                    ),
                    5
                ),
                isDisplayed()
            )
        )
        Thread.sleep(5000)
        textView2.check(matches(withText("Descripción")))

        val textView3 = onView(
            allOf(
                withText("Producto"),
                childAtPosition(
                    allOf(
                        withId(R.id.action_bar),
                        childAtPosition(
                            withId(R.id.action_bar_container),
                            0
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )

        onView(isRoot()).perform(ViewActions.pressBack())
        Thread.sleep(5000)
        onView(isRoot()).perform(ViewActions.pressBack())



        val searchAutoComplete3 = onView(
            allOf(
                withId(R.id.search_src_text),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete3.perform(replaceText("aknddgrg"), closeSoftKeyboard())

        val searchAutoComplete4 = onView(
            allOf(
                withId(R.id.search_src_text), withText("aknddgrg"),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete4.perform(pressImeActionButton())

        val textView4 = onView(
            allOf(
                withId(R.id.alertTitle), withText("No encontramos publicaciones"),
                childAtPosition(
                    allOf(
                        withId(R.id.title_template),
                        childAtPosition(
                            withId(R.id.topPanel),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )

        Thread.sleep(5000)
        val frameLayout = onView(
            allOf(IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java), isDisplayed())
        )


        onView(withText("No encontramos publicaciones")).check(matches(isDisplayed()))
        onView(withText("Revisa que la palabra este bien escrita.")).check(matches(isDisplayed()))

        val appCompatButton = onView(
            allOf(
                withId(android.R.id.button1), withText("OK"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.buttonPanel),
                        0
                    ),
                    3
                )
            )
        )
        appCompatButton.perform(scrollTo(), click())

        val imageView = onView(
            allOf(
                withId(R.id.imageView),
                childAtPosition(
                    allOf(
                        withId(R.id.main),
                        childAtPosition(
                            withId(R.id.container),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        imageView.check(matches(isDisplayed()))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
