package pruebaml.example.com.pruebaml


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest3 {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest3() {
        val searchAutoComplete = onView(
            allOf(
                withId(R.id.search_src_text),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete.perform(replaceText("jbl"), closeSoftKeyboard())

        val searchAutoComplete2 = onView(
            allOf(
                withId(R.id.search_src_text), withText("jbl"),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete2.perform(pressImeActionButton())
        Thread.sleep(10000)


        var exist = false

        while (!exist){

            try {
                onView(withText("Parlante Jbl Flip 4 Bluetooth - Consultar Colores!")).perform(click())
                // View is in hierarchy
                exist = true
            } catch (e: NoMatchingViewException) {
                // View is not in hierarchy\
                onView(withId(R.id.recycler_view)).perform(swipeUp())
            }catch (e:Exception){
                onView(withId(R.id.recycler_view)).perform(swipeUp())
            }

        }
        onView(withText("Parlante Jbl Flip 4 Bluetooth - Consultar Colores!")).perform(click())

        Thread.sleep(10000)
        val textView = onView(
            allOf(
                withId(R.id.product_title), withText("Parlante Jbl Flip 4 Bluetooth - Consultar Colores!"),
                childAtPosition(
                    allOf(
                        withId(R.id.main_bar_constraint),
                        childAtPosition(
                            withId(R.id.product_main_frame),
                            0
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Parlante Jbl Flip 4 Bluetooth - Consultar Colores!")))

        val textView2 = onView(
            allOf(
                withId(R.id.product_price), withText("U$ 165"),
                childAtPosition(
                    allOf(
                        withId(R.id.main_bar_constraint),
                        childAtPosition(
                            withId(R.id.product_main_frame),
                            0
                        )
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        textView2.check(matches(withText("U$ 165")))

        pressBack()

        pressBack()

        val searchAutoComplete3 = onView(
            allOf(
                withId(R.id.search_src_text),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete3.perform(replaceText("pelota"), closeSoftKeyboard())

        val searchAutoComplete4 = onView(
            allOf(
                withId(R.id.search_src_text), withText("pelota"),
                childAtPosition(
                    allOf(
                        withId(R.id.search_plate),
                        childAtPosition(
                            withId(R.id.search_edit_frame),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        searchAutoComplete4.perform(pressImeActionButton())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
