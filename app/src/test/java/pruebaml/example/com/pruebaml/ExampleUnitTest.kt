package pruebaml.example.com.pruebaml

import androidx.annotation.NonNull
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.junit.Test

import org.junit.Assert.*
import pruebaml.example.com.pruebaml.api.NetworkService
import pruebaml.example.com.pruebaml.utils.Utils
import retrofit2.HttpException
import java.net.HttpURLConnection
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.disposables.Disposable
import io.reactivex.Scheduler
import org.junit.Before
import org.junit.BeforeClass
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }


    @Test
    fun priceCurrencyTest() {
        var price = Utils.getPriceWithCurrency(33.99,"UYU")
        assertEquals("$ 34", price)

        price = Utils.getPriceWithCurrency(33.91109,"UYU")
        assertEquals("$ 34", price)

        price = Utils.getPriceWithCurrency(18.0,"USD")
        assertEquals("U$ 18", price)

        price = Utils.getPriceWithCurrency(18.0,"")
        assertEquals("$ 18", price)


    }
}
