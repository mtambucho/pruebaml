Picasso 
Junto con Glide es una de las bibliotecas mas usadas para cargar imágenes, ambas son recomendadas por Google en las conferencias Android, son muy rapidas y trabajan en segundo plano haciendo la app mas fluida.

Retrofit 
Es un cliente REST bastante simple. Permite hacer las peticiones GET, POST, PUT, PATCH, DELETE y HEAD; gestionar diferentes tipos de parámetros y parsear automáticamente la respuesta.

Utilice las librerías de Android Jetpack incluidas en 2018, tanto los componentes Pagina, Navigation, Live Data y View Model. No es lo que utilizo habitualmente pero sí ya las había utilizado en algún proyecto y me pareció una buena practica.

